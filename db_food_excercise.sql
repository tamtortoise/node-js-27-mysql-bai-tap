-- Adminer 4.8.1 MySQL 8.0.31 dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

SET NAMES utf8mb4;

DROP TABLE IF EXISTS `food`;
CREATE TABLE `food` (
  `food_id` int NOT NULL AUTO_INCREMENT,
  `food_name` varchar(255) COLLATE utf8mb4_vi_0900_ai_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_vi_0900_ai_ci DEFAULT NULL,
  `price` int DEFAULT NULL,
  `desc` varchar(255) COLLATE utf8mb4_vi_0900_ai_ci DEFAULT NULL,
  `type_id` int DEFAULT NULL,
  PRIMARY KEY (`food_id`),
  KEY `food_ibfk_1` (`type_id`),
  CONSTRAINT `food_ibfk_1` FOREIGN KEY (`type_id`) REFERENCES `food_type` (`type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_vi_0900_ai_ci;

INSERT INTO `food` (`food_id`, `food_name`, `image`, `price`, `desc`, `type_id`) VALUES
(1,	'Coca',	'',	10,	'nước ngọt',	1),
(2,	'Sting Dâu',	'',	10,	'nước ngọt',	1),
(3,	'Burger',	'',	35,	'Burger Bò trứng',	2),
(4,	'Hủ tiếu',	'',	30,	'hủ tiếu nam vang',	3),
(5,	'Bún bò',	'',	50,	'bún bò đặc biệt',	3),
(6,	'Bít tết',	'',	100,	'Bò bít tết kèm khoai tây chiên',	2),
(7,	'Bò Kho',	'',	45,	'Bò kho bánh mì',	2),
(8,	'Đồ chay',	'',	15,	'Bún xào chay',	5),
(9,	'Bia',	'',	15,	'Heiniken',	4),
(10,	'Trà đá',	'',	5,	'Nước uống',	4);

DROP TABLE IF EXISTS `food_type`;
CREATE TABLE `food_type` (
  `type_id` int NOT NULL AUTO_INCREMENT,
  `type_name` varchar(255) COLLATE utf8mb4_vi_0900_ai_ci DEFAULT NULL,
  PRIMARY KEY (`type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_vi_0900_ai_ci;

INSERT INTO `food_type` (`type_id`, `type_name`) VALUES
(1,	'Thức uống'),
(2,	'Thức ăn nhanh'),
(3,	'Món nước'),
(4,	'Ăn vặt'),
(5,	'Chay');

DROP TABLE IF EXISTS `like_res`;
CREATE TABLE `like_res` (
  `user_id` int NOT NULL,
  `res_id` int NOT NULL,
  `date_like` datetime DEFAULT NULL,
  PRIMARY KEY (`user_id`,`res_id`),
  KEY `res_id` (`res_id`),
  CONSTRAINT `like_res_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`),
  CONSTRAINT `like_res_ibfk_2` FOREIGN KEY (`res_id`) REFERENCES `restaurant` (`res_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_vi_0900_ai_ci;

INSERT INTO `like_res` (`user_id`, `res_id`, `date_like`) VALUES
(1,	1,	'2023-01-01 00:00:00'),
(1,	2,	'2023-01-01 00:00:00'),
(1,	3,	'2023-01-01 00:00:00'),
(2,	1,	'2023-01-01 00:00:00'),
(2,	2,	'2023-01-01 00:00:00'),
(2,	3,	'2023-01-01 00:00:00'),
(3,	1,	'2023-01-01 00:00:00'),
(3,	2,	'2023-01-01 00:00:00'),
(4,	2,	'2023-01-01 00:00:00'),
(4,	3,	'2023-01-01 00:00:00'),
(5,	3,	'2023-01-01 00:00:00'),
(6,	3,	'2023-01-01 00:00:00'),
(7,	1,	'2023-01-01 00:00:00'),
(7,	2,	'2023-01-01 00:00:00'),
(7,	3,	'2023-01-01 00:00:00'),
(8,	3,	'2023-01-01 00:00:00');

DROP TABLE IF EXISTS `order_food`;
CREATE TABLE `order_food` (
  `user_id` int NOT NULL,
  `food_id` int NOT NULL,
  `amount` int DEFAULT NULL,
  `code` varchar(255) COLLATE utf8mb4_vi_0900_ai_ci DEFAULT NULL,
  `arr_sub_id` varchar(255) COLLATE utf8mb4_vi_0900_ai_ci DEFAULT NULL,
  PRIMARY KEY (`user_id`,`food_id`),
  KEY `food_id` (`food_id`),
  CONSTRAINT `order_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`),
  CONSTRAINT `order_ibfk_2` FOREIGN KEY (`food_id`) REFERENCES `food` (`food_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_vi_0900_ai_ci;

INSERT INTO `order_food` (`user_id`, `food_id`, `amount`, `code`, `arr_sub_id`) VALUES
(1,	1,	3,	'',	'[1,2]'),
(1,	2,	2,	'',	'[4,5]'),
(3,	1,	1,	'',	''),
(3,	4,	1,	'',	''),
(3,	5,	5,	'',	''),
(3,	8,	10,	'',	''),
(3,	9,	10,	'',	'[1,2,3]'),
(3,	10,	10,	'',	'');

DROP TABLE IF EXISTS `rate_res`;
CREATE TABLE `rate_res` (
  `user_id` int NOT NULL,
  `res_id` int NOT NULL,
  `amount` int DEFAULT NULL,
  `date_rate` datetime DEFAULT NULL,
  PRIMARY KEY (`user_id`,`res_id`),
  KEY `res_id` (`res_id`),
  CONSTRAINT `rate_res_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`),
  CONSTRAINT `rate_res_ibfk_2` FOREIGN KEY (`res_id`) REFERENCES `restaurant` (`res_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_vi_0900_ai_ci;

INSERT INTO `rate_res` (`user_id`, `res_id`, `amount`, `date_rate`) VALUES
(1,	2,	4,	'2023-01-01 00:00:00'),
(1,	3,	5,	'2023-01-01 00:00:00'),
(2,	1,	3,	'2023-01-01 00:00:00'),
(2,	3,	3,	'2023-01-01 00:00:00');

DROP TABLE IF EXISTS `restaurant`;
CREATE TABLE `restaurant` (
  `res_id` int NOT NULL AUTO_INCREMENT,
  `res_name` varchar(255) COLLATE utf8mb4_vi_0900_ai_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_vi_0900_ai_ci DEFAULT NULL,
  `desc` varchar(255) COLLATE utf8mb4_vi_0900_ai_ci DEFAULT NULL,
  PRIMARY KEY (`res_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_vi_0900_ai_ci;

INSERT INTO `restaurant` (`res_id`, `res_name`, `image`, `desc`) VALUES
(1,	'Texas',	'',	'Gà Texas'),
(2,	'KFC',	'',	'Gà Kfc'),
(3,	'Bò Ba Cô',	'',	'BBC');

DROP TABLE IF EXISTS `sub_food`;
CREATE TABLE `sub_food` (
  `sub_id` int NOT NULL AUTO_INCREMENT,
  `sub_name` varchar(255) COLLATE utf8mb4_vi_0900_ai_ci DEFAULT NULL,
  `sub_price` int DEFAULT NULL,
  `food_id` int DEFAULT NULL,
  PRIMARY KEY (`sub_id`),
  KEY `food_id` (`food_id`),
  CONSTRAINT `sub_food_ibfk_1` FOREIGN KEY (`food_id`) REFERENCES `food` (`food_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_vi_0900_ai_ci;

INSERT INTO `sub_food` (`sub_id`, `sub_name`, `sub_price`, `food_id`) VALUES
(1,	'Hành phi',	1,	4),
(2,	'Hành phi',	1,	5),
(3,	'Hành phi',	1,	8),
(4,	'Trân châu',	2,	1),
(5,	'Trân châu',	2,	2),
(6,	'tương ớt',	2,	3),
(7,	'tương ớt',	2,	10);

DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `user_id` int NOT NULL AUTO_INCREMENT,
  `full_name` varchar(255) COLLATE utf8mb4_vi_0900_ai_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_vi_0900_ai_ci DEFAULT NULL,
  `pass_word` varchar(255) COLLATE utf8mb4_vi_0900_ai_ci DEFAULT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_vi_0900_ai_ci;

INSERT INTO `user` (`user_id`, `full_name`, `email`, `pass_word`) VALUES
(1,	'Mitasu',	'mitasu@gmail.com',	'1234'),
(2,	'Lucoste',	'lucoste@gmail.com',	'1234'),
(3,	'Nick',	'nick@gmail.com',	'1234'),
(4,	'Coach',	'coach@gmail.com',	'1234'),
(5,	'Rochelle',	'bleh bleh',	'123456'),
(6,	'',	'',	''),
(7,	'abc',	'abc@gmail.com',	'$2b$10$tkFIqg.QZbwr33SBNaf4re0HXC6bIrRf/49PwDWa.ulh/8bkXVKXy'),
(8,	'graphql',	'grap@gmail.com',	'1234'),
(9,	'graphql',	'grap@gmail.com',	'1234'),
(12,	'graphql',	'grap@gmail.com',	'1234');

-- 2023-01-21 07:27:52
